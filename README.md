# UDELAR - Facultad de Ingeniería
# Introducción a la Ciencia de Datos - Tarea 1

## Descripción

Este repositorio contiene un Python notebook con la implementación de todos los puntos descriptos en la Tarea 1 de la materia Introducción a Ciencia de Datos correspondiente a la Maestría en Ciencia de Datos y Aprendizaje Automático.

Se adjunta también un documento donde se exponen los detalles de cada una de las partes que comprenden la Tarea 1

## Referencias

Indroducción a la Ciencia de Datos y Aprendizaje Automático: https://eva.fing.edu.uy/course/view.php?id=1378
Tarea 1: https://gitlab.fing.edu.uy/maestria-cdaa/intro-cd/-/blob/master/Tarea_1/shakespeare_propuesta.ipynb

# Ejecución del Notebook

Para utilizar el notebook, se recomienda subirlo a la plataforma Google Colab, abrirlo y comenzar a ejecutar las celdas una a una desde la primera en donde se instalarán todas las librerías necesarias para la ejecución del código.